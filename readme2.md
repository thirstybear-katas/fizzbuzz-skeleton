## Step 2

Additionally a number is now 'fizz' if it is divisible by 3 _or if it has a 3 in it_

For example, 13 => _Fizz_


When complete, [go to Step 3](https://gitlab.com/thirstybear/fizzbuzz-skeleton/blob/master/readme3.md)...